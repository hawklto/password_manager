import 'package:flutter/material.dart';
import 'package:password_manager/constants.dart';
import 'package:password_manager/cryptography.dart';
import 'package:password_manager/widgets/button_data.dart';
import 'package:password_manager/data_storage.dart';
import 'package:password_manager/widgets/custom_form_field.dart';
import 'package:password_manager/widgets/custom_form_field_with_label.dart';
import 'package:password_manager/widgets/password_dialog.dart';

void main() => runApp(const PasswordManager());

class PasswordManager extends StatelessWidget {
  const PasswordManager({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: kthemeData,
      home: const PasswordManagerScreen(),
    );
  }
}

class PasswordManagerScreen extends StatefulWidget {
  const PasswordManagerScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<PasswordManagerScreen> createState() => _PasswordManagerScreenState();
}

class _PasswordManagerScreenState extends State<PasswordManagerScreen> {
  List<Widget>? dataList;
  @override
  Widget build(BuildContext context) {
    Cryptography cryptography = Cryptography();
    DataStorage dataStorage = DataStorage();

    void refreshScreenDelayed() {
      Future.delayed(const Duration(milliseconds: 200), () {
        setState(() {});
      });
    }

    void addPasswordPopUp() {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertForms(refreshScreen: refreshScreenDelayed);
          }).whenComplete(() {
        refreshScreenDelayed();
      });
    }

    return FutureBuilder<List>(
        future: dataStorage.readWebsites(),
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          } else {
            final List data = snapshot.data!;
            List<Widget> dataWidgetsList = [];
            for (var element in data) {
              dataWidgetsList.add(ButtonData(
                website: element["website"],
                index: element["index"],
                refresh: refreshScreenDelayed,
              ));
            }
            dataWidgetsList.add(
              TextButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: const Text("Refresh")),
            );
            dataWidgetsList.add(
              TextButton(
                  onPressed: () {
                    dataStorage.deleteAllData();
                    setState(() {});
                  },
                  child: const Text("Erase all data")),
            );
            dataWidgetsList.add(TextButton(
                onPressed: () {
                  print(cryptography.decrypt(cryptography.encrypt("Dionathan")));
                },
                child: Text("Testing")));
            print("dataWidget" + dataWidgetsList.toString());

            return Scaffold(
              appBar: AppBar(
                title: const Text("Password Manager"),
                centerTitle: true,
              ),
              body: ListView(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                padding: const EdgeInsets.only(top: 30.0, right: 30.0, left: 30.0),
                children: dataWidgetsList,
              ),
              floatingActionButton: TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.white, shape: const CircleBorder()),
                onPressed: () {
                  addPasswordPopUp();
                },
                child: const Icon(
                  Icons.add,
                  size: 35.0,
                  color: Color(0xFF161626),
                ),
              ),
            );
          }
        });
  }
}

class AlertForms extends StatelessWidget {
  VoidCallback refreshScreen;

  AlertForms({required this.refreshScreen});

  var websiteController = TextEditingController();
  var loginController = TextEditingController();
  var passwordController = TextEditingController();
  DataStorage dataStorage = DataStorage();

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      backgroundColor: kBackgroundColor,
      titlePadding: const EdgeInsets.all(24),
      title: const Text(
        "New password data",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 28.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      contentPadding: const EdgeInsets.only(left: 30.0, right: 30.0),
      children: [
        SizedBox(
          height: 340.0,
          width: 300.0,
          child: ListView(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            children: [
              CustomFormFieldWithLabel(hintText: "Site", controller: websiteController),
              CustomFormFieldWithLabel(hintText: "Login", controller: loginController),
              CustomFormFieldWithLabel(
                hintText: "Senha",
                controller: passwordController,
                isPassword: true,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: TextButton(
              onPressed: () {
                TextEditingController passwordDataController = TextEditingController();
                if (websiteController.text.replaceAll(" ", "").isNotEmpty &&
                    loginController.text.replaceAll(" ", "").isNotEmpty &&
                    passwordController.text.replaceAll(" ", "").isNotEmpty) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return PasswordDialog(
                            label: "Set password data",
                            passwordDataController: passwordDataController,
                            onTap: () {
                              Map data = {
                                "website": websiteController.text,
                                "password": passwordDataController.text,
                                "data": {
                                  "login": loginController.text,
                                  "password": passwordController.text,
                                }
                              };
                              dataStorage.writeData(data);
                              Navigator.pop(context);
                            });
                      }).whenComplete(() {
                    Navigator.pop(context);
                  });
                } else {
                  print("Faltou preencher algumas coisas hein");
                }
              },
              child: const Text(
                "Ok",
                style: TextStyle(fontSize: 24.0),
              )),
        )
      ],
    );
  }
}
